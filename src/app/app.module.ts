import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IotApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { DevicesPage } from '../pages/devices/devices';
import { NewDevicePage } from '../pages/devices/device.new';
import { EditDevicePage } from '../pages/devices/device.edit';
import { SchedulesDevicePage } from '../pages/devices/device.schedules';
import { NewScheduleDevicePage } from '../pages/devices/device.schedule.new';


@NgModule({
  declarations: [
    IotApp,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    DevicesPage,
    NewDevicePage,
    EditDevicePage,
    SchedulesDevicePage,
    NewScheduleDevicePage
  ],
  imports: [
    IonicModule.forRoot(IotApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    IotApp,
    HomePage,
    DevicesPage,
    ProfilePage,
    LoginPage,
    RegisterPage,
    NewDevicePage,
    EditDevicePage,
    SchedulesDevicePage,
    NewScheduleDevicePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
