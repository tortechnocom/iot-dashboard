import { Component, ViewChild } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Platform, MenuController, Nav, AlertController} from 'ionic-angular';

import { StatusBar, Splashscreen } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { DevicesPage } from '../pages/devices/devices';

import {LoginService} from '../services/login.service';
import {APIServices} from '../services/api.services';


declare var Paho: any;
declare var Plotly: any;

@Component({
  templateUrl: 'app.html',
  providers: [LoginService, APIServices]
})
export class IotApp {
  @ViewChild(Nav) nav: Nav;
  // public
  public static apiUrl:string = "https://iot-backend.popstacksoft.com/popbackend/control/callServiceApi";
  public static httpOptions: RequestOptions;
  public static user: any;
  public static http: any;
  public static Paho: any;
  // make HelloIonicPage the root (or first) page
  rootPage: any;
  currentPage: any;
  loggedIn: boolean;
  pages: Array<{title: string, component: any, visible: boolean, icon: string}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public http: Http,
    private loginService: LoginService,
    private apiServices: APIServices,
    public alertCtrl: AlertController
  ) {
    IotApp.Paho = Paho;
    IotApp.http = this.http;
    //this.rootPage = HomePage;
    // set our app's pages
    this.pages = [
      { title: 'Home', component: HomePage, visible: true, icon: "home"},
      { title: 'Devices', component: DevicesPage, visible: false, icon: "cloud"},
      { title: 'Profile', component: ProfilePage, visible: false, icon: "person"},
      { title: 'Register', component: RegisterPage, visible: true, icon: "create"},
      { title: 'Login', component: LoginPage, visible: true, icon: "lock"}
    ];
    this.currentPage = HomePage;
    for(let index in this.pages) {
      if (this.pages[index].title == localStorage.getItem('currentPage')) {
        this.currentPage = this.pages[index].component;
        break;
      }
    }
    this.loggedIn = this.checkLogin();
    this.refreshMenu();

    // set app global
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
    /**
     * @see "Replacement for $scope.emit() or $scope.broadcast() in Angular 2 using a shared service to handle events"
     * http://stackoverflow.com/questions/34700438/global-events-in-angular-2
     */
    this.loginService.onLogin.subscribe(
      event => {
        localStorage.setItem("token", event);
        this.loggedIn = true;
        this.refreshMenu();
        this.currentPage = DevicesPage;
        localStorage.setItem('currentPage', 'Devices');
        this.checkLogin();
      }
    );
    this.loginService.onLogout.subscribe(
      event => {
        localStorage.removeItem('token');
        this.currentPage = HomePage;
        this.loggedIn = event;
        this.menu.close();
        this.refreshMenu();
        this.nav.setRoot(LoginPage);
    });
  }
  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
  //if (page.title != 'Login') {
      localStorage.setItem("currentPage", page.title);
    //}
    this.nav.setRoot(page.component);
  }
  checkLogin() {
    let accessToken = localStorage.getItem('token');
    if (accessToken) {
      let headers = new Headers({
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': accessToken});
        IotApp.httpOptions = new RequestOptions({ headers: headers});
        let params = JSON.stringify({'service': 'iotGetUser'});
        IotApp.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
          .map(res => res.json())
          .subscribe(
            data => {
              let title = "Respone";
              if (data.responseMessage == "error" || data.errorMessage != null) {
                let alert = this.alertCtrl.create({
                  title: title,
                  subTitle: data.errorMessage,
                  buttons: ['OK']
                });
                alert.present();
                this.loginService.onLogout.emit(false);
              } else {
                IotApp.user = data.user;
                this.nav.setRoot(this.currentPage);
              }
            },
            err => console.log('Error: ', err),
            () => console.log('Set User Completed')
        );
        return true;
    } else {
        IotApp.httpOptions = null;
        IotApp.user = null;
        this.rootPage = this.currentPage;
        return false;
    }
  }
  logout () {
    let params = JSON.stringify({service: "popLogoutApi", data: {token: localStorage.getItem('token')}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.loginService.onLogout.emit(false);
        },
        err => console.log("Error: ", err),
        () => console.log('Logout Completed')
    );
    
  }
  private refreshMenu() {
    if (this.loggedIn == false) { // Logged out
      for (var page of this.pages) {
        if (page.title == "Login" || page.title == "Home" || page.title == "Register") {
          page.visible = true;
        } else {
          page.visible = false;
        }
      }
    } else { // Logged in
      for (var page of this.pages) {
        if (page.title == "Login" || page.title == "Register") {
          page.visible = false;
        } else {
          page.visible = true;
        }
      }
    }
  }
  public static checkLoginApi() {
    let params = JSON.stringify({service: "popCheckLoginApi", data: {token: localStorage.getItem('token')}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          if (data.isExpired != "N") {
            localStorage.removeItem("token");
            localStorage.removeItem("currentPage");
            window.location.reload(true);
          }
        },
        err => console.log("Error: ", err),
        () => console.log('Checked Login')
    );
  }
}

