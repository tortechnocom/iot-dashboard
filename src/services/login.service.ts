import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class LoginService {

  /**
   * @see "Replacement for $scope.emit() or $scope.broadcast() in Angular 2 using a shared service to handle events"
   * http://stackoverflow.com/questions/34700438/global-events-in-angular-2
   */
  public onLogin: EventEmitter<Object>;

  public onLogout: EventEmitter<Object>;

  constructor() {
    this.onLogin = new EventEmitter<Object>();
    this.onLogout = new EventEmitter<Object>();
  }
}