import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { NewDevicePage } from './device.new';
import { EditDevicePage } from './device.edit';
import { SchedulesDevicePage } from './device.schedules';
import {Http} from '@angular/http';
import {IotApp} from '../../app/app.component';


@Component({
  selector: 'devices-page',
  templateUrl: 'devices.html'
})
export class DevicesPage {
  private deviceList: any;
  public deviceMap: any[] = [];
  private mqttClient: any;
  constructor(public navCtrl: NavController,
    public http: Http,
    public alertCtrl: AlertController
    ) {
    localStorage.setItem("reconnect", "true");
    //check login api
    IotApp.checkLoginApi();
    //mqtt
    let options = {
      onSuccess: this.onConnect,
      userName: IotApp.user.mqtt.username,
      password: IotApp.user.mqtt.password,
      useSSL: true,
      invocationContext: this,
       keepAliveInterval: 300
    }
    this.mqttClient = new IotApp.Paho.MQTT.Client(
      IotApp.user.mqtt.serverUrl,
      Number(IotApp.user.mqtt.serverWebSocketPort),
      "iot-dashboard-" + IotApp.user.partyId + "-" + new Date().getTime()
      );
      this.mqttClient.onConnectionLost = function () {
        //console.log("==>onConnectionLost: ");
        this.connectOptions.invocationContext.reconnect();
      }
      this.mqttClient.onMessageArrived = function (message) {
      //get device list
      let params = JSON.stringify({service: "iotGetDeviceList"});
      IotApp.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
        .map(res => res.json())
        .subscribe(
          data => {
            this.connectOptions.invocationContext.deviceList = data.deviceList;
          },
          err => console.log("Error: ", err),
          () => {}
      );
    }
    this.mqttClient.connect(options);
    //get device list
    let params = JSON.stringify({service: "iotGetDeviceList"});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.deviceList = data.deviceList;
          if (this.mqttClient.isConnected()) {
            this.subscribe(IotApp.user.mqtt.topic);
          }
          for (let key in this.deviceList) {
            this.deviceMap[this.deviceList[key].deviceId] = this.deviceList[key];
          }
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
  newDevice() {
    this.navCtrl.push(NewDevicePage);
  }
  removeDevice(deviceId, deviceName) {
    let confirm = this.alertCtrl.create({
      title: 'Remove Device',
      message: 'Do you want to remove Device [' + deviceName +']?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Remove',
          handler: () => {
            let params = JSON.stringify({service: "iotRemoveDevice", data: {deviceId: deviceId}});
            this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
              .map(res => res.json())
              .subscribe(
                data => {
                  let params = JSON.stringify({service: "iotGetDeviceList"});
                  this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
                    .map(res => res.json())
                    .subscribe(
                      data => {
                        this.deviceList = data.deviceList;
                      },
                      err => console.log("Error: ", err),
                      () => console.log('Authentication Completed')
                  );
                },
                err => console.log("Error: ", err),
                () => console.log('Authentication Completed')
            );
          }
        }
      ]
    });
    confirm.present();
  }
  editDevice(deviceId) {
    localStorage.setItem('reconnect', 'false');
    this.navCtrl.push(EditDevicePage, {deviceId: deviceId});
  }
  schedules(deviceId) {
    localStorage.setItem("reconnect", "false");
    this.navCtrl.push(SchedulesDevicePage, {deviceId: deviceId});
  }
  subscribe(topic) {
    var subscribeOptions = {
        qos: 0,  // QoS
        invocationContext: {foo: true},  // Passed to success / failure callback
        onSuccess: this.onSuccessCallback,
        onFailure: this.onFailureCallback,
        timeout: 300
    }
    this.mqttClient.subscribe(topic, subscribeOptions);
  }
  publish(device, event) {
    let text = "";
    if (event.checked) {
      text = "ON";
    } else {
      text = "OFF";
    }
    let message = new IotApp.Paho.MQTT.Message(text);
    message.destinationName = device.mqtt.topic;
    message.qos = 0;
    
    // set device playload
    let params = JSON.stringify({service: "iotSetDevicePlayload", data: {
      deviceId: device.deviceId, playload: text}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.mqttClient.send(message);
        },
        err => console.log("Error: ", err),
        () => {}
    );
  }
  onSuccessCallback () {
    //console.log("==> onSuccessCallback");
  }
  onFailureCallback () {
    //console.log("==> onFailureCallback");
  }
  onConnect() {
    //console.log("==> onConnect")
  }
  reconnect() {
    if (localStorage.getItem('reconnect') == 'true') {
      this.navCtrl.setRoot(DevicesPage);
    }
  }
}
