import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {NavController, AlertController} from 'ionic-angular';
import {IotApp} from '../../app/app.component';
import {DevicesPage} from './devices';
import { EditDevicePage } from './device.edit';


@Component({
  selector: 'device-new-page',
  templateUrl: 'device.new.html'
})
export class NewDevicePage {
  private input: any = {
    deviceName: "",
    description: "",
    deviceTypeId: ""
  };
  private deviceTypeList: any;
  constructor(public navCtrl: NavController, public http: Http, public alertCtrl: AlertController) {
    let params = JSON.stringify({service: "iotGetDeviceTypeList", data: this.input});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          console.log("deviceTypeList: ", data.deviceTypeList);
          this.deviceTypeList = data.deviceTypeList;
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
  
  goBack() {
    this.navCtrl.pop();
  }
  addDevice() {
    let params = JSON.stringify({service: "iotAddDevice", data: this.input});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          let title = "Respone: " + data.responseMessage
          if (data.responseMessage == "error" || data.errorMessage != null) {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: data.errorMessage,
              buttons: ['OK']
            });
            alert.present();
          } else {
            this.navCtrl.setRoot(DevicesPage);
            this.navCtrl.push(EditDevicePage,{
              deviceId: data.deviceId
            });
          }
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
  cancelDevice() {
    this.navCtrl.setRoot(DevicesPage);
  }
}