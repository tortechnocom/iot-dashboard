import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {NavController, AlertController, NavParams, ToastController, ModalController, ViewController} from 'ionic-angular';
import {IotApp} from '../../app/app.component';


@Component({
  selector: 'device-schedule-new-page',
  templateUrl: 'device.schedule.new.html'
})
export class NewScheduleDevicePage {
  private frequencyList: any;
  private actionList: any;
  private deviceId: any;
  private maxDate: any;
  private input: any = {
    scheduleName: "",
    description: "",
    frequency: "",
    deviceId: "",
    startDate: "",
    endDate: "",
    interval: "",
    count: -1
  };
  constructor(public navCtrl: NavController,
    public http: Http,
    public alertCtrl: AlertController, public toastCtrl: ToastController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController) {
    
    let now = new Date();

    let oneYr = new Date();
    oneYr.setFullYear(now.getFullYear() + 0);
    this.maxDate = oneYr.toISOString();
    IotApp.checkLoginApi();
    this.input.deviceId = navParams.get("deviceId");
    let params = JSON.stringify({service: "iotGetDeviceScheduleSetting", data: {deviceId: this.deviceId}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.frequencyList = data.deviceScheduleLSetting.frequencyList;
          this.actionList = data.deviceScheduleLSetting.actionList;
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );

  }
  cancel() {
    localStorage.setItem("reconnect", "true");
    this.viewCtrl.dismiss();
  }
  createDeviceSchedule(){
    let browserStartDate = new Date(this.input.startDate);
    let actualStartTime = browserStartDate.getTime() + (browserStartDate.getTimezoneOffset() * 60 * 1000);
    this.input.startDate = new Date(actualStartTime);
    let browserEndDate = new Date(this.input.endDate);
    let actualEndTime = browserEndDate.getTime() + (browserEndDate.getTimezoneOffset() * 60 * 1000);
    this.input.endDate = new Date(actualEndTime);
    let params = JSON.stringify({service: "iotCreateDeviceSchedule", data: this.input});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.viewCtrl.dismiss();
          localStorage.setItem('reconnect', 'true');
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
}