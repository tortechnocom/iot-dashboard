import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {NavController, AlertController, NavParams, ToastController} from 'ionic-angular';
import {IotApp} from '../../app/app.component';
import {DevicesPage} from './devices';


@Component({
  selector: 'device-edit-page',
  templateUrl: 'device.edit.html'
})
export class EditDevicePage {
  public deviceId: string;
  private input: any = {
    deviceId: "",
    deviceName: "",
    description: "",
    deviceTypeId: "",
    mqtt: {}
  };
  private deviceTypeList: any;
  constructor(public navCtrl: NavController,
    public http: Http,
    public alertCtrl: AlertController, public toastCtrl: ToastController,
    public navParams: NavParams) {
    IotApp.checkLoginApi();
    this.deviceId = navParams.get("deviceId");
    let params = JSON.stringify({service: "iotGetDeviceTypeList", data: this.input});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.deviceTypeList = data.deviceTypeList;
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );

    params = JSON.stringify({service: "iotGetDevice", data: {deviceId: this.deviceId}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.input = data.device;
          console.log("device: ", this.input);
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );

  }
  
  goBack() {
    this.navCtrl.pop();
  }
  updateDevice() {
    this.input.deviceId = this.deviceId;
    let params = JSON.stringify({service: "iotUpdateDevice", data: this.input});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          let title = "Respone: " + data.responseMessage
          if (data.responseMessage == "error" || data.errorMessage != null) {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: data.errorMessage,
              buttons: ['OK']
            });
            alert.present();
          } else {
            let toast = this.toastCtrl.create({
              message: title,
              duration: 3000
            });
            toast.present();
            this.navCtrl.setRoot(DevicesPage);
          }
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
  cancelDevice() {
    this.navCtrl.setRoot(DevicesPage);
  }
}