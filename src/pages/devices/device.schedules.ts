import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {NavController, AlertController, NavParams, ToastController, ModalController} from 'ionic-angular';
import {IotApp} from '../../app/app.component';
import { NewScheduleDevicePage } from './device.schedule.new';


@Component({
  selector: 'device-schedules-page',
  templateUrl: 'device.schedules.html'
})
export class SchedulesDevicePage {
  private deviceScheduleList: any;
  private deviceId: any;
  constructor(public navCtrl: NavController,
    public http: Http,
    public alertCtrl: AlertController, public toastCtrl: ToastController,
    public navParams: NavParams,
    public modalCtrl: ModalController) {

    IotApp.checkLoginApi();
    this.deviceId = navParams.get("deviceId");
    this.getDeviceScheduleList();
  }
  getDeviceScheduleList(){
    let params = JSON.stringify({service: "iotGetDeviceScheduleList", data: {deviceId: this.deviceId}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.deviceScheduleList = data.deviceScheduleList;
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
  newDeviceSchedule(deviceId) {
    let modal = this.modalCtrl.create(NewScheduleDevicePage, {deviceId: deviceId});
    modal.present();
  }
  disableDeviceSchedule(jobId) {
    let params = JSON.stringify({service: "iotDisableDeviceSchedule", data: {jobId: jobId}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.getDeviceScheduleList();
        },
        err => console.log("Error: ", err),
        () =>{}
    );
  }
  enableDeviceSchedule(jobId) {
    let params = JSON.stringify({service: "iotEnableDeviceSchedule", data: {jobId: jobId}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.getDeviceScheduleList();
        },
        err => console.log("Error: ", err),
        () =>{}
    );
  }
  deleteDeviceSchedule(jobId) {
    let params = JSON.stringify({service: "iotDeleteDeviceSchedule", data: {jobId: jobId}});
    this.http.post(IotApp.apiUrl, params , IotApp.httpOptions)
      .map(res => res.json())
      .subscribe(
        data => {
          this.getDeviceScheduleList();
        },
        err => console.log("Error: ", err),
        () =>{}
    );
  }
}