import { Component } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {IotApp} from '../../app/app.component';
import {NavController, AlertController} from 'ionic-angular';
import {LoginService} from '../../services/login.service';
import {LoginPage} from '../../pages/login/login';


@Component({
  selector: 'register-page',
  templateUrl: 'register.html'
})
export class RegisterPage {
  private input: any = {
    firstName: "",
    lastName: "",
    email: "",
    currentPassword: "",
    currentPasswordVerify: ""
  };
  constructor(
    public http: Http, 
    public nav: NavController,
    public loginService: LoginService,
    public alertCtrl: AlertController
    ) {

  }
  
  register() {
    let headers = new Headers({'Content-Type': 'application/json', 'Accept' : 'application/json'});
    let options = new RequestOptions({ headers: headers});
    let params = JSON.stringify({service: "iotRegisterUser", data: this.input});
    this.http.post(IotApp.apiUrl, params , options)
      .map(res => res.json())
      .subscribe(
        data => {
          let title = "Respone: " + data.responseMessage
          if (data.responseMessage == "error") {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: data.errorMessage,
              buttons: ['OK']
            });
            alert.present();
          } else {
            let alert = this.alertCtrl.create({
              title: title,
              subTitle: data.successMessage,
              buttons: ['OK']
            });
            alert.present();
            this.nav.setRoot(LoginPage);
          }
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
}
