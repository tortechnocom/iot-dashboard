import { Component } from '@angular/core';
import {IotApp} from '../../app/app.component';

@Component({
  selector: 'profile-page',
  templateUrl: 'profile.html'
})

export class ProfilePage {
    user: any;
    showPassword = false;
    constructor() {
        IotApp.checkLoginApi();
        this.user = IotApp.user;
    }
    seePassword() {
        this.showPassword = true;
    }
    hidePassword() {
        this.showPassword = false;
    }
}