import { Component, ViewChild } from '@angular/core';
import { Headers, RequestOptions} from '@angular/http'
import {IotApp} from '../../app/app.component';

declare var Plotly: any;

@Component({
  selector: 'home-page',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('graphContainer') graphContainer;
  private devices = 0;
  constructor() {
     //get globol devices
      let headers = new Headers({'Content-Type': 'application/json', 'Accept' : 'application/json'});
      let options = new RequestOptions({ headers: headers});
      let params = JSON.stringify({service: "iotGetGlobDevices"});
      IotApp.http.post(IotApp.apiUrl, params , options)
        .map(res => res.json())
        .subscribe(
          data => {
            this.devices = data.devices
            var self = this;
            var d3 = Plotly.d3;
            var WIDTH_IN_PERCENT_OF_PARENT = 100,
                HEIGHT_IN_PERCENT_OF_PARENT = 50;

            var gd3 = d3.select(self.graphContainer.nativeElement)
                .append('div')
                .style({
                    width: WIDTH_IN_PERCENT_OF_PARENT + '%',
                    'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',
                    height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh',
                    'margin-top': (50 - HEIGHT_IN_PERCENT_OF_PARENT) / 2 + 'vh'
                });

            var gd = gd3.node();
            var dataChart = [{
              values: [100],
              labels: ['Glob Devices'],
              domain: {
                y: [0, 1],
                x: [0, 1]
              },
              marker: {
                colors: ['orange']
              },
              name: 'Devices',
              hoverinfo: 'name',
              hole: .9,
              type: 'pie',
              showlegend: false,
              textinfo: 'none'
            }];
            var layout = {
              paper_bgcolor: 'rgba(0,0,0,0)',
              plot_bgcolor: 'rgba(0,0,0,0)',
              annotations: [
                {
                  font: {
                    size: 60
                  },
                  showarrow: false,
                  text: this.devices + "",
                  x: 0.5,
                  y: 0.5
              }],
              width: (window.screen.width * 0.9)
            };
            Plotly.newPlot(gd, dataChart, layout);
            window.onresize = function() {
              Plotly.Plots.resize(gd);
            };
          },
          err => console.log("Error: ", err),
          () => {}
      );
  }
}
