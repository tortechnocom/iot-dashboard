import { Component } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {IotApp} from '../../app/app.component';
import {NavController, AlertController} from 'ionic-angular';
import {LoginService} from '../../services/login.service';


@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  private input: any = {
    username: "",
    password: ""
  };
  private rememberMe = false;
  constructor(
    public http: Http, 
    public nav: NavController,
    public loginService: LoginService,
    public alertCtrl: AlertController
    ) {
      if (localStorage.getItem("rememberMe")) {
        this.rememberMe = (localStorage.getItem("rememberMe") == 'true');
      }
      if (this.rememberMe == true) {
        this.input = {
          username: localStorage.getItem("username"),
          password: localStorage.getItem("password")
        }
      }
      

  }
  
  login() {
    let headers = new Headers({'Content-Type': 'application/json', 'Accept' : 'application/json'});
    let options = new RequestOptions({ headers: headers});
    let params = JSON.stringify({service: "popLoginApi", data: this.input});
    this.http.post(IotApp.apiUrl, params , options)
      .map(res => res.json())
      .subscribe(
        data => {
          if (data.responseMessage == "success") {
            if (this.rememberMe == true) {
              localStorage.setItem('rememberMe', 'true');
              localStorage.setItem('username', this.input['username']);
              localStorage.setItem('password', this.input['password']);
            } else {
              localStorage.removeItem('rememberMe');
              localStorage.removeItem('username');
              localStorage.removeItem('password');
            }
            this.loginService.onLogin.emit(data.token);
          } else {
            let alert = this.alertCtrl.create({
              title: data.responsMessage,
              subTitle: data.errorMessage,
              buttons: ['OK']
            });
            alert.present();
          }
        },
        err => console.log("Error: ", err),
        () => console.log('Authentication Completed')
    );
  }
}
